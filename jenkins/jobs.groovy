String baseDir = "sealions-prj" // ??
String gitUrl = "https://gitlab.com/angelin/test-gradle-prj.git"

folder(baseDir){
    description "This is basic dir for jobs"
}

job('build-job'){
    scm {
        git {
            remote {
                url(gitUrl)
                branch("master")
            }
        }
    }
  
    customWorkspace('/tmp/workspace')

    steps {
        shell('echo START')
        gradle('clean')
        gradle('build')
    }

    publishers {
        extendedEmail {
            recipientList('cc:team.sealions@gmail.com,cc:andreyangelin01@gmail.com')
            defaultSubject('Jenkins notification')
            defaultContent('Something broken')
            contentType('text/html')
            sendTo {
                recipientList()
            }
            triggers {
                failure()
            }
        }
    }
}