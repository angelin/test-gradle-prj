package com.nixsolutions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SealionsScheduleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SealionsScheduleServiceApplication.class, args);
	}
}
